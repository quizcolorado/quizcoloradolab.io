import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Quiz } from '../quiz';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.sass']
})
export class QuizComponent implements OnInit, AfterContentChecked {

  constructor(private quizService: QuizService) { }

  contentLoaded: boolean;
  quiz: Quiz;

  ngOnInit() {
    this.contentLoaded = false;
    this.quizService.getQuiz().subscribe(result => {
      this.quiz = result;
      this.contentLoaded = true;
    });
  }

  ngAfterContentChecked() {
    console.log('hello there');
    console.log(this.contentLoaded);
  }

}
