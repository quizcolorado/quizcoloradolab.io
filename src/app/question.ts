export class Question {
    question: string;
    choices: string[];
    answer: string;
}
